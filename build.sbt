import sbt.Keys.resolvers
import sbt._
import Dependencies._

organization := "com.eniro"
name := "TX2S3Importer"
version := "0.1-SNAPSHOT"
scalaVersion in ThisBuild := "2.12.12"

//The default SBT testing java options are too small to support running many of the tests
javaOptions in Test ++= Seq(
  "-Xms512M",
  "-Xmx2048M",
  "-XX:MaxMetaspaceSize=500m",
  "-XX:+CMSClassUnloadingEnabled",
  "-Duser.timezone=UTC"
)
fork in Test := true
fork in run := true

// 1. This is where SBT can reach out to resolve dependencies. SBT uses Apache Ivy to resolve dependencies by default, but can work with Maven repositories as well
resolvers += Resolver.sbtPluginRepo( "releases" )
resolvers += Classpaths.typesafeReleases
resolvers in ThisBuild ++= Seq(
  //("Maven Central Legacy HTTP" at "http://insecure.repo1.maven.org/maven2/").withAllowInsecureProtocol(true),
  "Maven Central" at "https://repo1.maven.org/maven2/",
  "Spark Packages Repo" at "https://dl.bintray.com/spark-packages/maven",
  "Sonatype releases" at "https://oss.sonatype.org/content/repositories/releases",
  "Sonatype OSS Release" at "https://oss.sonatype.org/content/groups/public/",
  "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
  "Artima Maven Repository" at "http://repo.artima.com/releases",
  "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/",
  "Eniro repository" at "https://artifactory.eniro.com/ext-release-local/"
)

// 2. Define the class to run when calling "java -jar ..."
mainClass in(Compile, run) := Some( "com.eniro.manyana.Main" )

// 3. Add the project dependencies, see project/Dependencies.scala for dependency management
libraryDependencies ++= spark ++ logging ++ uzhttp ++ configParser ++ zio ++ mssql ++ misc

// 4. Deployment of this artifact should be part of a CI/CD pipeline. Running the unit tests while building the "fat jar" is very expensive,
//    therefore, don't do it during the "assembly" phase (which will be run on Openshift).
test in assembly := {}

// 5. Resolve any conflicts when merging into a "fat jar"
assemblyMergeStrategy in assembly := {
  case PathList( "META-INF", "MANIFEST.MF" ) => MergeStrategy.discard
  case PathList( "reference.conf" ) => MergeStrategy.concat
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.last
}

scalacOptions ++= Seq("-feature", "-Ypartial-unification", "-deprecation")

// http://www.wartremover.org/
//wartremoverErrors ++= Warts.unsafe
