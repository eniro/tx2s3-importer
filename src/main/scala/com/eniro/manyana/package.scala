package com.eniro

import com.typesafe.scalalogging.Logger
import uzhttp.{Response, Status}
import zio.Task

import org.apache.spark.sql.{Column, functions => f}
import java.nio.charset.{Charset, StandardCharsets}

import pureconfig._
import pureconfig.generic.auto._

case class Env(name: String) extends AnyVal

case class Port(number: Int) extends AnyVal

case class SparkConfig(
    master: String,
    parquetStorage: String,
    metricsNs: String
)

case class TxConfig(
    serverName: String,
    databaseName: String,
    tableName: String,
    username: String,
    password: String
)

case class BucketConfig(
    server: String,
    accessKey: String,
    secretKey: String
)

case class AppConfig(
    env: Env,
    port: Port,
    spark: SparkConfig,
    tx: TxConfig,
    bucket: BucketConfig
)

package object manyana {

  val logError: Logger => String => Task[Unit] = logger =>
    msg => Task.effect(logger.error(msg))
  val logInfo: Logger => String => Task[Unit] = logger =>
    msg => Task.effect(logger.info(msg))

  def json(
      body: String,
      status: Status = Status.Ok,
      headers: List[(String, String)] = Nil,
      charset: Charset = StandardCharsets.UTF_8
  ): Response =
    Response.const(
      body.getBytes(charset),
      status,
      contentType = s"application/json; charset=${charset.name()}",
      headers = headers
    )

  val config: AppConfig = ConfigSource.default.load[AppConfig] match {
    case Right(config) => config
    case Left(err)     => {
      err.toList.foreach(println)
      AppConfig(
        Env("dev"),
        Port(8080),
        SparkConfig("local[4]", "metrics", "/tmp"),
        TxConfig("jdbc:sqlserver://localhost:1433", "manana", "dbo.Manyana_Events", "", ""),
        BucketConfig("", "", "")
      )
    }
  }

}
