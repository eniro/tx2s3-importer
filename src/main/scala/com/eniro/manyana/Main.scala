package com.eniro.manyana

import com.typesafe.scalalogging.StrictLogging
import uzhttp.server.Server
import uzhttp.{HTTPError, Request, Response}
import zio.{App, ExitCode, IO, Task, URIO, ZEnv, ZIO}
import zio.blocking.{Blocking, effectBlocking}

import java.net.InetSocketAddress
import uzhttp.HTTPError.{InternalServerError, NotFound}

object Main extends App with StrictLogging {

  val successJson = """{"status":"sucess"}"""

  logger.info(s"${config.tx}")
  logger.info(s"${config.spark}")

  def getParam(q: String, key: String): Option[String] =
    if (q == null) None
    else {
      q.split("&")
        .map(s => s.split("="))
        .filter(arr => arr(0) == key)
        .headOption match {
        case None    => None
        case Some(a) => Some(a(1))
      }
    }

  val routes: Request => ZIO[Blocking, HTTPError, Response] = {
    case req if req.uri.getPath == "/start" => {
      val q = req.uri.getQuery
      val countryCode = getParam(q, "cc").getOrElse("DK")

      val eff = effectBlocking(TXImporter.run(countryCode))
        .bimap(
          t => {
            val msg = s"--- ${t.getLocalizedMessage}"
            logger.error(s"$msg\n${t.getStackTrace.mkString("\n")}")
            HTTPError.InternalServerError(msg)
          },
          _ => json(successJson)
        )
        .catchAll(t =>
          Task((t.getStackTrace.toList.map(st => logger.error(s"> $st"))))
        )
        .fork

      eff *> URIO(json(successJson))
    }

    case req if req.uri.getPath == "/stop" =>
      effectBlocking(TXImporter.stop())
        .bimap(
          t => {
            val msg = s"--- ${t.getLocalizedMessage}"
            logger.error(s"$msg\n${t.getStackTrace.mkString("\n")}")
            HTTPError.InternalServerError(msg)
          },
          _ => json(successJson)
        )

    case req => IO.fail(NotFound(req.uri.toString))
  }

  def run(args: List[String]): URIO[ZEnv, ExitCode] =
    Server
      .builder(new InetSocketAddress("0.0.0.0", config.port.number))
      .handleAll[ZEnv](routes)
      .serve
      .useForever
      .orDie
}
