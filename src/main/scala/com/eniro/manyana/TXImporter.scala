package com.eniro.manyana

import com.typesafe.scalalogging.StrictLogging

import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkConf
import org.apache.spark.sql.Encoders
import org.apache.spark.sql.{DataFrame, SparkSession, functions => f}
import org.apache.spark.sql.types.TimestampType

import java.sql.Timestamp
import org.apache.spark.sql.{Dataset, Row, SaveMode}

object TXImporter extends StrictLogging {
  logger.info("TXImporter instantiating")
  private val createSparkconf: SparkConf =
    new SparkConf()
      .setAppName("TX Importer")
      .setMaster(config.spark.master)
      .set("spark.metrics.namespace", config.spark.metricsNs)
      .set("spark.sql.session.timeZone", "UTC")
      .set("spark.hadoop.fs.s3a.connection.ssl.enabled", "false")
      .set("spark.hadoop.fs.s3a.endpoint", config.bucket.server)
      .set("spark.hadoop.fs.s3a.access.key", config.bucket.accessKey)
      .set("spark.hadoop.fs.s3a.secret.key", config.bucket.secretKey)
      .set("spark.hadoop.fs.s3a.path.style.access", "true")
      .set(
        "spark.hadoop.fs.s3a.impl",
        "org.apache.hadoop.fs.s3a.S3AFileSystem"
      )
      .set(
        "spark.hadoop.fs.s3a.aws.credentials.provider",
        "org.apache.hadoop.fs.s3a.SimpleAWSCredentialsProvider"
      )

  val spark = SparkSession.builder
    .config(createSparkconf)
    .getOrCreate()

  import spark.implicits._

  logger.info("TXImporter ready")

  def run(countryCode: String) = {
    val jdbcUrl =
      s"${config.tx.serverName};databaseName=${config.tx.databaseName};"
    logger.info(s"jdbcUrl: $jdbcUrl")
    val jdbcDF = spark.read
      .format("com.microsoft.sqlserver.jdbc.spark")
      .option("url", jdbcUrl)
      .option("dbtable", config.tx.tableName)
      .option("driver", "com.microsoft.sqlserver.jdbc.SQLServerDriver")
      .option("user", config.tx.username)
      .option("password", config.tx.password)
      .option("schemaCheckEnabled", false)
      .load()
      .where(
        'LocationCountryCode === countryCode && 'AccountStatus === "Active"
      )

    logger.info("About to show")
    jdbcDF.show(5, false)

    jdbcDF
      .repartition('LocationCountryCode)
      .write
      .partitionBy("DW_Batch", "LocationCountryCode")
      .mode(SaveMode.Overwrite)
      .parquet(s"s3a://${config.spark.parquetStorage}")

    spark.stop()
  }

  def stop(): Unit =
    spark.stop()
}
