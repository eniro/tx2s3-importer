import sbt._

object Dependencies {

  val sparkVersion = "3.0.1"
  val hadoopVersion = "2.10.1"
  val sparkTestBaseVersion = "2.4.3_0.12.0"
  val zioVersion = "1.0.1"

  final val SPARK_ENV = "ENV"
  final val APP_ARGS = "APP_ARGS"

  val envStr: String = sys.env.get(APP_ARGS) match {
    case Some(v) => v
    case None    => sys.env.getOrElse(SPARK_ENV, "dev")
  }

  val spark_local = Seq(
    "org.apache.spark" %% "spark-core" % sparkVersion,
    "org.apache.spark" %% "spark-sql" % sparkVersion
  )

  val spark_non_local = Seq(
    "org.apache.spark" %% "spark-core" % sparkVersion % "provided",
    "org.apache.spark" %% "spark-sql" % sparkVersion % "provided"
  )

  val spark_common = Seq(
    "org.apache.hadoop" % "hadoop-aws" % hadoopVersion,
    "org.apache.hadoop" % "hadoop-common" % hadoopVersion
  )

  val spark = envStr match {
    case "dev" | "local" => spark_local ++ spark_common
    case _               => spark_non_local ++ spark_common
  }

  val logging = Seq(
    "ch.qos.logback" % "logback-classic" % "1.2.3",
    "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"
  )

  val configParser = Seq(
    "com.github.pureconfig" %% "pureconfig" % "0.14.1"
  )

  val zio = Seq(
    "dev.zio" %% "zio" % zioVersion
  )

  val mssql = Seq(
    "com.microsoft.sqlserver" % "mssql-jdbc" % "7.2.1.jre8",
    "com.microsoft.azure" % "spark-mssql-connector_2.12" % "1.1.0"
    //"com.eniro" % "spark-mssql-connector-assembly" % "1.0.0"
  )

  val uzhttp = Seq(
    "org.polynote" %% "uzhttp" % "0.2.5"
  )

  val misc = Seq(
    "commons-validator" % "commons-validator" % "1.6",
    "commons-configuration" % "commons-configuration" % "1.10",
    //tests
    "org.scalactic" %% "scalactic" % "3.0.3" % "test",
    "com.holdenkarau" %% "spark-testing-base" % sparkTestBaseVersion % "test"
    //"MrPowers" % "spark-fast-tests" % "0.21.1-s_2.12" % "test"
  )
}
